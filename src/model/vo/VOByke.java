package model.vo;

import java.util.Date;

/**
 * Representation of a byke object
 */
public class VOByke implements Comparable<VOByke>{
	private int id;

	public VOByke(int id) {
		super();
		this.id = id;
	}

	public int darId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int compareTo(VOByke o) {
		if(id > o.darId()){
			return 1;
		}else if(id > o.darId()){
			return -1;
		}
		else
			return 0;
	}
}



