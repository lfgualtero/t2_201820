package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import api.IDivvyTripsManager;
import au.com.bytecode.opencsv.CSVReader;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.LinkedList;
import model.data_structures.Node;
//test234
public class DivvyTripsManager implements IDivvyTripsManager {

	private LinkedList stations;
	private LinkedList trips;


	public DivvyTripsManager() {
		super();
		this.stations = new LinkedList<>();
		this.trips = new LinkedList<>();
	}


	public void loadStations (String stationsFile) {
		String csvFile = stationsFile;
		BufferedReader br = null;
		String line = "";
		//Se define separador ","

		String cvsSplitBy = ",";
		try {
			br = new BufferedReader(new FileReader(csvFile));
			br.readLine();


			DateFormat format = new SimpleDateFormat("MM/dd/YYYY hh:mm:ss");

			while ((line = br.readLine()) != null) {                
				String[] datos = line.split(cvsSplitBy);
				//Imprime datos.
				try {

					stations.add(new VOStation(Integer.parseInt(datos[0]), datos[1], datos[2], Double.parseDouble(datos[3]), Double.parseDouble(datos[4]), Integer.parseInt(datos[5]), datos[6]));

				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}


	public void loadTrips (String tripsFile) {
		String csvFile = tripsFile;
		BufferedReader br = null;
		String line = "";
		//Se define separador ","

		String cvsSplitBy = ",";
		try {
			br = new BufferedReader(new FileReader(csvFile));
			br.readLine();


			while ((line = br.readLine()) != null) {                
				String[] datos = line.split(cvsSplitBy);
				//Imprime datos.
				try {


					
					if(datos.length==10)
					{
						trips.add(new VOTrip(Integer.parseInt(datos[0]), datos[1], datos[2], Integer.parseInt(datos[3]), Integer.parseInt(datos[4]), Integer.parseInt(datos[5]),datos[6], Integer.parseInt(datos[7]), datos[8],datos[9]));
					}else if(datos.length==11)
					{
						trips.add(new VOTrip(Integer.parseInt(datos[0]), datos[1], datos[2], Integer.parseInt(datos[3]), Integer.parseInt(datos[4]), Integer.parseInt(datos[5]),datos[6], Integer.parseInt(datos[7]), datos[8],datos[9], datos[10]));
					}else if(datos.length==12)
					{
						trips.add(new VOTrip(Integer.parseInt(datos[0]), datos[1], datos[2], Integer.parseInt(datos[3]), Integer.parseInt(datos[4]), Integer.parseInt(datos[5]),datos[6], Integer.parseInt(datos[7]), datos[8],datos[9], datos[10], Integer.parseInt(datos[11])));
					}
					
				} catch (NumberFormatException e) {

					e.printStackTrace();
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
//		VOTrip trip1= null;
		LinkedList<VOTrip> ls=new LinkedList<VOTrip>();
//		int i=0;
//		int size=trips.getSize();
//		
//		while(!trips.getElement(i).equals(null)&& i<=size){
//			
//			trip1=(VOTrip) trips.getElement(i);
//			i++;
//			String genero=trip1.getGender();
//			
//			if(trip1!= null && genero!=null  && genero.equalsIgnoreCase(gender) ){
//				ls.add(trip1);
//				System.out.println(size);
//				System.out.println(i);
//				System.out.println(trip1);
//			}
//		}
//		return ls;
		
		Node<VOTrip> nodo = trips.getHeadNode();
		while(nodo!=null)
		{
			VOTrip trip1=(VOTrip) nodo.darElemento();
			String genero=trip1.getGender();
			
			if(trip1!= null   && genero!=null  && genero.equalsIgnoreCase(gender) )
			{
				ls.add(trip1);
			}
			nodo = nodo.darSiguiente();
		}
		
		return ls;
	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationIDp) {
		//VOTrip trip1= null;
		LinkedList<VOTrip> ls=new LinkedList<VOTrip>();
		/*
		int i=0;
		int size=trips.getSize();
		
		while(!trips.getElement(i).equals(null)&& i<=size){
			
			trip1=(VOTrip) trips.getElement(i);
			i++;
			int stationId=trip1.getTo_station_id();
			
			if(trip1!= null   && stationId==stationIDp ){
				ls.add(trip1);
				System.out.println(size);
				System.out.println(i);
				System.out.println(trip1);
			}
		}*/
		Node<VOTrip> nodo = trips.getHeadNode();
		while(nodo!=null)
		{
			VOTrip trip1=(VOTrip) nodo.darElemento();
			int stationId=trip1.getTo_station_id();
			
			if(trip1!= null   && stationId==stationIDp )
			{
				ls.add(trip1);
			}
			nodo = nodo.darSiguiente();
		}
		
		return ls;
	}	


}