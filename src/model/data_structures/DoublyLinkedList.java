package model.data_structures;

import java.util.Collection;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface DoublyLinkedList<T> extends Iterable<T> {

	
	public boolean add(T t);
	
	public T delete( int index);
	
	public int getSize();

	public T getElement(int index);

	public T element();
	
	public boolean addAtEnd(T e);
}
